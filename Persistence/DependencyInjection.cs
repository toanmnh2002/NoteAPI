﻿using Application;
using Application.Interfaces.IRepositories;
using Application.Interfaces.IServices;
using Application.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Persistence.Repositories;
using System.Text.Json.Serialization;

namespace Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection PersistenceServices(this IServiceCollection services, IConfiguration configuration)
        {
            #region JsonConfig
            services.AddControllers().AddJsonOptions(opt =>
            {
                opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                opt.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
            });
            #endregion

            #region MapperConfig
            services.AddAutoMapper(typeof(DependencyInjection).Assembly);
            //services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            #endregion

            #region CoreServices
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<INoteService, NoteService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            #endregion

            #region DbConfig
            services.AddDbContext<AppDbContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("NoteDB")));
            #endregion

            return services;
        }
    }
}
