﻿using Application.Extensions;
using Application.Models.AccountModels;
using Application.Models.NoteModels;
using Application.Models.OrderModels;
using Application.Models.UserModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enum;


namespace Persistence.AutoMapper
{
    public class MapperConfiguration : Profile
    {
        public MapperConfiguration()
        {
            #region Pagination
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
            #endregion

            #region Note
            CreateMap<CreateNoteViewModel, Note>().ReverseMap();
            CreateMap<UpdateNoteViewModel, Note>().ReverseMap();
            CreateMap<Note, NoteViewModel>()
                .ForMember(d => d.NoteId, s => s.MapFrom(x => x.Id))
                .ReverseMap();
            #endregion

            #region User
            CreateMap<LoginModel, User>().ReverseMap();
            CreateMap<RegisterModel, User>().ForMember(d => d.Role, s => s.MapFrom(x => RoleName.Customer))
                .ForMember(d => d.Status, s => s.MapFrom(x => StatusEnum.Active)).ReverseMap();
            CreateMap<User, UserViewModel>();
            #endregion
            CreateMap<OrderModel, Order>().ReverseMap();
            CreateMap<CreateOrderModel, Order>()
                .ForMember(d => d.OrderStatus, s => s.MapFrom(x => OrderStatus.Processing)).ReverseMap()
                .ReverseMap();
            CreateMap<OrderDetails, OrderDetail>().ReverseMap();

            CreateMap<CreateOrderDetail, OrderDetail>().ReverseMap();
            CreateMap<CreateOrderModel, Order>().ReverseMap();

        }
    }
}
