﻿//using Applications;
//using AutoMapper;
//using Domain.Entities;

//namespace Infrastructures.AutoMapper
//{
//    public class ResolverMappingGeneric<TSource, TDestination> : IValueResolver<TSource, TDestination, string>
//    where TSource : BaseEntity
//    where TDestination : class
//    {
//        private readonly IUnitOfWork _unitOfWork;

//        public ResolverMappingGeneric(IUnitOfWork unitOfWork)
//        {
//            _unitOfWork = unitOfWork;
//        }

//        public string Resolve(TSource source, TDestination destination, string destMember, ResolutionContext context)
//        {
//            if (source.DeletedBy == Guid.Empty || source.ModificatedBy == Guid.Empty)
//                return null;
//            if (source.IsDeleted is true)
//            {
//                var user = _unitOfWork.CustomerRepository.FirstOrDefaultAsync(x =>
//                 x.Id == source.DeletedBy).Result;
//                return user.UserName;
//            }
//            return null;
//        }
//    }
//}
