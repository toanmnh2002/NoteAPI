﻿using Domain.Entities;
using Domain.Enum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

namespace Persistence.FluentAPIs
{
    public class Configurations
    {
        public class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : BaseEntity
        {
            public virtual void Configure(EntityTypeBuilder<TEntity> builder)
            {
                builder.HasKey(x => x.Id);
                builder.Property(x => x.IsDeleted).HasDefaultValue(false);
                builder.HasQueryFilter(x => !x.IsDeleted);
            }
        }

        public class UserConfig : IEntityTypeConfiguration<User>
        {
            public void Configure(EntityTypeBuilder<User> builder)
            {
                builder.ToTable(nameof(User));
                builder.HasIndex(x => x.Email).IsUnique();
                builder.Property(u => u.Password).IsRequired().HasMaxLength(100);
                builder.Property(u => u.Phone).HasMaxLength(10);
                builder.HasData(new User
                {
                    Id = new Guid("00000001-0000-0000-0000-000000000000"),
                    Email = "toanmnh2002@gmail.com",
                    CreationDate = DateTime.UtcNow,
                    Password = "$2a$12$v2Agh6VLGNyWXLZeb4aH/eBjYYF7WtMxqsmJLxSvKPkB4v.9pNzse",
                    Phone = "086927346x",
                    Status = StatusEnum.Active,
                    Role = RoleName.Admin,
                    Name = "ToanNguyen"
                },
                new User
                {
                    Id = new Guid("00000002-0000-0000-0000-000000000000"),
                    Email = "toanmanh2002@gmail.com",
                    CreationDate = DateTime.UtcNow,
                    Password = "$2a$12$v2Agh6VLGNyWXLZeb4aH/eBjYYF7WtMxqsmJLxSvKPkB4v.9pNzse",
                    Phone = "086927346x",
                    Status = StatusEnum.Active,
                    Role = RoleName.Admin,
                    Name = "ToanNguyen"
                });
            }
        }

        public class NoteConfiguration : IEntityTypeConfiguration<Note>
        {
            public void Configure(EntityTypeBuilder<Note> builder)
            {

                builder.Property(p => p.Image)
                       .HasConversion(listOfStringImages => JsonConvert.SerializeObject(listOfStringImages),
                                      imagesJsonRepresentation => JsonConvert.DeserializeObject<List<string>>(imagesJsonRepresentation));

                builder.HasMany(x => x.OrderDetails)
                       .WithOne(x => x.Note)
                       .HasForeignKey(x => x.NoteId);
                builder.HasData(new Note
                {
                    Id = new Guid("00000003-0000-0000-0000-000000000000"),
                    Title = "This is about ToanNguyen",
                    Content = "PRObi",
                    Price = 100,
                    Quantity = 100,
                    CreationDate = DateTime.UtcNow,
                    Descriptions = "Descriptiasdons",
                    Priority = PriorityEnum.HighPriority,
                    NoteStatus = NoteStatusEnum.Doing
                },
                new Note
                {
                    Id = new Guid("00000004-0000-0000-0000-000000000000"),
                    Title = "this",
                    Content = "PRObi",
                    Price = 1000,
                    Quantity = 100,
                    CreationDate = DateTime.UtcNow,
                    Descriptions = "Descriptionsasd",
                    Priority = PriorityEnum.HighPriority,
                    NoteStatus = NoteStatusEnum.Doing
                }, new Note
                {
                    Id = new Guid("00000005-0000-0000-0000-000000000000"),
                    Title = "This is ",
                    Content = "PRObi",
                    Price = 100000,
                    Quantity = 100,
                    CreationDate = DateTime.UtcNow,
                    Descriptions = "Descriptionsasd",
                    Priority = PriorityEnum.HighPriority,
                    NoteStatus = NoteStatusEnum.Doing,
                });
            }
        }

        public class OrderDetailTagConfiguration : IEntityTypeConfiguration<OrderDetail>
        {
            public void Configure(EntityTypeBuilder<OrderDetail> builder)
            {
                builder.HasKey(x => new { x.NoteId, x.OrderId });
                builder.Ignore(x => x.Id);
                builder.HasOne(x => x.Order)
                       .WithMany(x => x.OrderDetails)
                       .HasForeignKey(x => x.OrderId)
                       .OnDelete(DeleteBehavior.NoAction);

                builder.HasOne(x => x.Note)
                       .WithMany(x => x.OrderDetails)
                       .HasForeignKey(x => x.NoteId)
                       .OnDelete(DeleteBehavior.NoAction);

                builder.HasData(new OrderDetail
                {
                    DeletionDate = DateTime.UtcNow,
                    NoteId = new Guid("00000003-0000-0000-0000-000000000000"),
                    OrderId = new Guid("00000007-0000-0000-0000-000000000000"),
                    Quantity = 1
                },
                new OrderDetail
                {
                    DeletionDate = DateTime.UtcNow,
                    NoteId = new Guid("00000004-0000-0000-0000-000000000000"),
                    OrderId = new Guid("00000006-0000-0000-0000-000000000000"),
                    Quantity = 1
                });
            }
        }

        public class OrderConfiguration : IEntityTypeConfiguration<Order>
        {
            public void Configure(EntityTypeBuilder<Order> builder)
            {
                builder.HasOne(x => x.User)
                       .WithMany(x => x.Orders)
                       .HasForeignKey(x => x.UserId);
                builder.HasData(new Order
                {
                    Id = new Guid("00000006-0000-0000-0000-000000000000"),
                    UserId = new Guid("00000002-0000-0000-0000-000000000000"),
                    CreationDate = DateTime.UtcNow,
                    PaymentMethod = "Momo",
                    TotalPrice = 1000,
                    PaymentStatus = "Success",
                    OrderStatus = OrderStatus.Pending
                },
                new Order
                {
                    Id = new Guid("00000007-0000-0000-0000-000000000000"),
                    UserId = new Guid("00000002-0000-0000-0000-000000000000"),
                    CreationDate = DateTime.UtcNow,
                    PaymentMethod = "Momo",
                    TotalPrice = 100,
                    PaymentStatus = "Success",
                    OrderStatus = OrderStatus.Completed
                }
                );
            }
        }
    }
}
