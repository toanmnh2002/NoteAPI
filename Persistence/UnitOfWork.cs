﻿using Application;
using Application.Interfaces.IRepositories;

namespace Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IUserRepository _userRepository;
        private readonly INoteRepository _noteRepository;
        private readonly IOrderRepository _orderRepository;

        public UnitOfWork(AppDbContext context, IUserRepository userRepository, INoteRepository noteRepository, IOrderRepository orderRepository)
        => (_context, _userRepository, _noteRepository, _orderRepository) = (context, userRepository, noteRepository, orderRepository);
        public IUserRepository UserRepository => _userRepository;

        public INoteRepository NoteRepository => _noteRepository;

        public IOrderRepository OrderRepository => _orderRepository;
        public async Task<int> SaveChangesAsync() => await _context.SaveChangesAsync();
    }
}
