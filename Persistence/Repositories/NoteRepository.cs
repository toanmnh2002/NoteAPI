﻿using Application.Interfaces.IExtensions;
using Application.Interfaces.IRepositories;
using Domain.Entities;

namespace Persistence.Repositories
{
    public class NoteRepository : GenericRepository<Note>, INoteRepository
    {
        public NoteRepository(AppDbContext context, IClaimService claimService) : base(context, claimService)
        {
        }
    }
}
