﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Persistence.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImageURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Role = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModificationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ModificatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletionDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Note",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Descriptions = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Priority = table.Column<int>(type: "int", nullable: false),
                    NoteStatus = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModificationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ModificatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletionDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Note", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Note_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrderStatus = table.Column<int>(type: "int", nullable: false),
                    PaymentStatus = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PaymentMethod = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModificationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ModificatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletionDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderDetail",
                columns: table => new
                {
                    OrderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NoteId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModificationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ModificatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletionDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetail", x => new { x.NoteId, x.OrderId });
                    table.ForeignKey(
                        name: "FK_OrderDetail_Note_NoteId",
                        column: x => x.NoteId,
                        principalTable: "Note",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrderDetail_Order_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Order",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Note",
                columns: new[] { "Id", "Content", "CreatedBy", "CreationDate", "DeletedBy", "DeletionDate", "Descriptions", "Image", "IsDeleted", "ModificatedBy", "ModificationDate", "NoteStatus", "Price", "Priority", "Title", "UserId" },
                values: new object[,]
                {
                    { new Guid("00000003-0000-0000-0000-000000000000"), "PRObi", null, new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(3440), null, null, "Descriptiasdons", null, false, null, null, 0, 100m, 3, "This is about ToanNguyen", null },
                    { new Guid("00000004-0000-0000-0000-000000000000"), "PRObi", null, new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(3454), null, null, "Descriptionsasd", null, false, null, null, 0, 1000m, 3, "this", null },
                    { new Guid("00000005-0000-0000-0000-000000000000"), "PRObi", null, new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(3457), null, null, "Descriptionsasd", null, false, null, null, 0, 100000m, 3, "This is ", null }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "CreatedBy", "CreationDate", "DeletedBy", "DeletionDate", "Email", "ImageURL", "IsDeleted", "ModificatedBy", "ModificationDate", "Name", "Password", "Phone", "Role", "Status" },
                values: new object[,]
                {
                    { new Guid("00000001-0000-0000-0000-000000000000"), null, new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(7619), null, null, "toanmnh2002@gmail.com", null, false, null, null, "ToanNguyen", "$2a$12$v2Agh6VLGNyWXLZeb4aH/eBjYYF7WtMxqsmJLxSvKPkB4v.9pNzse", "086927346x", 0, 0 },
                    { new Guid("00000002-0000-0000-0000-000000000000"), null, new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(7627), null, null, "toanmanh2002@gmail.com", null, false, null, null, "ToanNguyen", "$2a$12$v2Agh6VLGNyWXLZeb4aH/eBjYYF7WtMxqsmJLxSvKPkB4v.9pNzse", "086927346x", 0, 0 }
                });

            migrationBuilder.InsertData(
                table: "Order",
                columns: new[] { "Id", "CreatedBy", "CreationDate", "DeletedBy", "DeletionDate", "IsDeleted", "ModificatedBy", "ModificationDate", "OrderStatus", "PaymentMethod", "PaymentStatus", "TotalPrice", "UserId" },
                values: new object[,]
                {
                    { new Guid("00000006-0000-0000-0000-000000000000"), null, new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(8895), null, null, false, null, null, 0, "Momo", "Success", 1000m, new Guid("00000002-0000-0000-0000-000000000000") },
                    { new Guid("00000007-0000-0000-0000-000000000000"), null, new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(8900), null, null, false, null, null, 2, "Momo", "Success", 100m, new Guid("00000002-0000-0000-0000-000000000000") }
                });

            migrationBuilder.InsertData(
                table: "OrderDetail",
                columns: new[] { "NoteId", "OrderId", "CreatedBy", "CreationDate", "DeletedBy", "DeletionDate", "IsDeleted", "ModificatedBy", "ModificationDate", "Quantity" },
                values: new object[,]
                {
                    { new Guid("00000003-0000-0000-0000-000000000000"), new Guid("00000007-0000-0000-0000-000000000000"), null, null, null, new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(2654), false, null, null, 1 },
                    { new Guid("00000004-0000-0000-0000-000000000000"), new Guid("00000006-0000-0000-0000-000000000000"), null, null, null, new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(2677), false, null, null, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Note_UserId",
                table: "Note",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_UserId",
                table: "Order",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetail_OrderId",
                table: "OrderDetail",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_User_Email",
                table: "User",
                column: "Email",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderDetail");

            migrationBuilder.DropTable(
                name: "Note");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
