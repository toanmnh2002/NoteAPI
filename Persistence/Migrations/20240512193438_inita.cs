﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Persistence.Migrations
{
    /// <inheritdoc />
    public partial class inita : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "Note",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Note",
                keyColumn: "Id",
                keyValue: new Guid("00000003-0000-0000-0000-000000000000"),
                columns: new[] { "CreationDate", "Quantity" },
                values: new object[] { new DateTime(2024, 5, 12, 19, 34, 38, 488, DateTimeKind.Utc).AddTicks(5783), 100 });

            migrationBuilder.UpdateData(
                table: "Note",
                keyColumn: "Id",
                keyValue: new Guid("00000004-0000-0000-0000-000000000000"),
                columns: new[] { "CreationDate", "Quantity" },
                values: new object[] { new DateTime(2024, 5, 12, 19, 34, 38, 488, DateTimeKind.Utc).AddTicks(5791), 100 });

            migrationBuilder.UpdateData(
                table: "Note",
                keyColumn: "Id",
                keyValue: new Guid("00000005-0000-0000-0000-000000000000"),
                columns: new[] { "CreationDate", "Quantity" },
                values: new object[] { new DateTime(2024, 5, 12, 19, 34, 38, 488, DateTimeKind.Utc).AddTicks(5793), 100 });

            migrationBuilder.UpdateData(
                table: "Order",
                keyColumn: "Id",
                keyValue: new Guid("00000006-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 19, 34, 38, 488, DateTimeKind.Utc).AddTicks(8072));

            migrationBuilder.UpdateData(
                table: "Order",
                keyColumn: "Id",
                keyValue: new Guid("00000007-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 19, 34, 38, 488, DateTimeKind.Utc).AddTicks(8080));

            migrationBuilder.UpdateData(
                table: "OrderDetail",
                keyColumns: new[] { "NoteId", "OrderId" },
                keyValues: new object[] { new Guid("00000003-0000-0000-0000-000000000000"), new Guid("00000007-0000-0000-0000-000000000000") },
                column: "DeletionDate",
                value: new DateTime(2024, 5, 12, 19, 34, 38, 489, DateTimeKind.Utc).AddTicks(4735));

            migrationBuilder.UpdateData(
                table: "OrderDetail",
                keyColumns: new[] { "NoteId", "OrderId" },
                keyValues: new object[] { new Guid("00000004-0000-0000-0000-000000000000"), new Guid("00000006-0000-0000-0000-000000000000") },
                column: "DeletionDate",
                value: new DateTime(2024, 5, 12, 19, 34, 38, 489, DateTimeKind.Utc).AddTicks(4740));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000001-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 19, 34, 38, 489, DateTimeKind.Utc).AddTicks(6958));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000002-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 19, 34, 38, 489, DateTimeKind.Utc).AddTicks(6963));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "Note");

            migrationBuilder.UpdateData(
                table: "Note",
                keyColumn: "Id",
                keyValue: new Guid("00000003-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(3440));

            migrationBuilder.UpdateData(
                table: "Note",
                keyColumn: "Id",
                keyValue: new Guid("00000004-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(3454));

            migrationBuilder.UpdateData(
                table: "Note",
                keyColumn: "Id",
                keyValue: new Guid("00000005-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(3457));

            migrationBuilder.UpdateData(
                table: "Order",
                keyColumn: "Id",
                keyValue: new Guid("00000006-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(8895));

            migrationBuilder.UpdateData(
                table: "Order",
                keyColumn: "Id",
                keyValue: new Guid("00000007-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 92, DateTimeKind.Utc).AddTicks(8900));

            migrationBuilder.UpdateData(
                table: "OrderDetail",
                keyColumns: new[] { "NoteId", "OrderId" },
                keyValues: new object[] { new Guid("00000003-0000-0000-0000-000000000000"), new Guid("00000007-0000-0000-0000-000000000000") },
                column: "DeletionDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(2654));

            migrationBuilder.UpdateData(
                table: "OrderDetail",
                keyColumns: new[] { "NoteId", "OrderId" },
                keyValues: new object[] { new Guid("00000004-0000-0000-0000-000000000000"), new Guid("00000006-0000-0000-0000-000000000000") },
                column: "DeletionDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(2677));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000001-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(7619));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("00000002-0000-0000-0000-000000000000"),
                column: "CreationDate",
                value: new DateTime(2024, 5, 12, 18, 34, 6, 94, DateTimeKind.Utc).AddTicks(7627));
        }
    }
}
