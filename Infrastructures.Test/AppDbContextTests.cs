﻿//using AutoFixture;
//using Domain.Entities;
//using Domain.Test;
//using FluentAssertions;
//using Microsoft.EntityFrameworkCore;

//namespace Infrastructures.Test
//{
//    public class AppDbContextTests : SetupTest, IDisposable
//    {
//        [Fact]
//        public async Task AppDbContext_UsersDbSetShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<User>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(10)
//                                   .ToList();

//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            var result = await _dbContext.Users.ToListAsync();

//            //assert
//            result.Should().BeEquivalentTo(mockData);
//        }

//        [Fact]
//        public async Task AppDbContext_UsersDbSetShouldReturnEmptyListWhenNotHavingData()
//        {
//            //act
//            var result = await _dbContext.Users.ToListAsync();

//            //assert
//            result.Should().BeEmpty();
//        }
//    }
//}
