﻿//using Applications.IRepositories;
//using AutoFixture;
//using Domain.Entities;
//using Domain.Test;
//using FluentAssertions;
//using Infrastructures.Repositories;

//namespace Infrastructures.Tests.Repositories
//{
//    public class GenericRepositoryTests : SetupTest
//    {
//        private readonly IGenericRepository<Customer> _genericRepository;
//        public GenericRepositoryTests()
//        {
//            _genericRepository = new GenericRepository<Customer>(
//                _dbContext,
//                _currentTimeMock.Object,
//                _claimServiceMock.Object);
//        }

//        [Fact]
//        public async Task GenericRepository_GetAllAsync_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(10)
//                                   .ToList();

//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            var result = await _genericRepository.GetAllAsync();

//            //assert
//            result.Should().BeEquivalentTo(mockData);
//        }


//        [Fact]
//        public async Task GenericRepository_GetAllAsync_ShouldReturnEmptyWhenHaveNoData()
//        {
//            //act
//            var result = await _genericRepository.GetAllAsync();

//            //assert
//            result.Should().BeEmpty();
//        }

//        [Fact]
//        public async Task GenericRepository_GetByIdAsync_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();

//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            var result = await _genericRepository.GetByIdAsync(mockData.Id);

//            //assert
//            result.Should().BeEquivalentTo(mockData);
//        }


//        [Fact]
//        public async Task GenericRepository_GetByIdAsync_ShouldReturnEmptyWhenHaveNoData()
//        {
//            //act
//            var result = await _genericRepository.GetByIdAsync(Guid.Empty);

//            //assert
//            result.Should().BeNull();
//        }

//        [Fact]
//        public async Task GenericRepository_AddAsync_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();

//            //act
//            await _genericRepository.AddEntityAsync(mockData);
//            var result = await _dbContext.SaveChangesAsync();

//            //assert
//            result.Should().Be(1);
//        }

//        [Fact]
//        public async Task GenericRepository_AddRangeAsync_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(10)
//                                   .ToList();

//            //act
//            await _genericRepository.AddRangeAsync(mockData);
//            var result = await _dbContext.SaveChangesAsync();

//            //assert
//            result.Should().Be(10);
//        }


//        [Fact]
//        public async Task GenericRepository_SoftDelete_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();
//            await _dbContext.Users.AddAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            _genericRepository.SoftDeleteEntity(mockData);
//            var result = await _dbContext.SaveChangesAsync();

//            result.Should().Be(1);
//        }

//        [Fact]
//        public async Task GenericRepository_Update_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();
//            await _dbContext.Users.AddAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            _genericRepository.Update(mockData);
//            var result = await _dbContext.SaveChangesAsync();

//            //assert
//            result.Should().Be(1);
//        }

//        [Fact]
//        public async Task GenericRepository_SoftRemoveRange_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(10)
//                                   .ToList();
//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            _genericRepository.SoftDeleteRange(mockData);
//            var result = await _dbContext.SaveChangesAsync();

//            //assert
//            result.Should().Be(10);
//        }

//        [Fact]
//        public async Task GenericRepository_UpdateRange_ShouldReturnCorrectData()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(10)
//                                   .ToList();
//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            _genericRepository.UpdateRange(mockData);
//            var result = await _dbContext.SaveChangesAsync();

//            //assert
//            result.Should().Be(10);
//        }

//        [Fact]
//        public async Task GenericRepository_ToPagination_ShouldReturnCorrectDataFirstsPage()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(45)
//                                   .ToList();
//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            var paginasion = await _genericRepository.ToPaginationAsync();

//            //assert
//            paginasion.PreviousPage.Should().BeFalse();
//            paginasion.NextPage.Should().BeTrue();
//            paginasion.Items.Count.Should().Be(10);
//            paginasion.TotalItemsCount.Should().Be(45);
//            paginasion.TotalPageCount.Should().Be(5);
//            paginasion.PageIndex.Should().Be(0);
//            paginasion.PageSize.Should().Be(10);
//        }

//        [Fact]
//        public async Task GenericRepository_ToPagination_ShouldReturnCorrectDataSecoundPage()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(45)
//                                   .ToList();
//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            var paginasion = await _genericRepository.ToPaginationAsync(1, 20);

//            //assert
//            paginasion.PreviousPage.Should().BeTrue();
//            paginasion.NextPage.Should().BeTrue();
//            paginasion.Items.Count.Should().Be(20);
//            paginasion.TotalItemsCount.Should().Be(45);
//            paginasion.TotalPageCount.Should().Be(3);
//            paginasion.PageIndex.Should().Be(1);
//            paginasion.PageSize.Should().Be(20);
//        }

//        [Fact]
//        public async Task GenericRepository_ToPagination_ShouldReturnCorrectDataLastPage()
//        {
//            //arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(45)
//                                   .ToList();
//            await _dbContext.Users.AddRangeAsync(mockData);
//            await _dbContext.SaveChangesAsync();

//            //act
//            var paginasion = await _genericRepository.ToPaginationAsync(2, 20);

//            //assert
//            paginasion.PreviousPage.Should().BeTrue();
//            paginasion.NextPage.Should().BeFalse();
//            paginasion.Items.Count.Should().Be(5);
//            paginasion.TotalItemsCount.Should().Be(45);
//            paginasion.TotalPageCount.Should().Be(3);
//            paginasion.PageIndex.Should().Be(2);
//            paginasion.PageSize.Should().Be(20);
//        }

//        [Fact]
//        public async Task GenericRepository_ToPagination_ShouldReturnWithoutData()
//        {
//            //act
//            var paginasion = await _genericRepository.ToPaginationAsync();

//            //assert
//            paginasion.PreviousPage.Should().BeFalse();
//            paginasion.NextPage.Should().BeFalse();
//            paginasion.Items.Count.Should().Be(0);
//            paginasion.TotalItemsCount.Should().Be(0);
//            paginasion.TotalPageCount.Should().Be(0);
//            paginasion.PageIndex.Should().Be(0);
//            paginasion.PageSize.Should().Be(10);
//        }
//    }
//}
