﻿//using Applications.ViewModels.UserViewModels;
//using AutoFixture;
//using Domain.Entities;
//using Domain.Test;
//using FluentAssertions;

//namespace Infrastructures.Test.MapperTests
//{
//    public class UserMapperTest : SetupTest
//    {
//        [Fact]
//        public void TestLoginUserViewModelMapper()
//        {
//            //arrange
//            var userMock = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();

//            //act
//            var result = _mapperConfig.Map<LoginUserViewModel>(userMock);

//            //assert
//            result.UserName.Should().Be(userMock.Username);
//        }

//        [Fact]
//        public void TestRegisterUserViewModelMapper()
//        {
//            //arrange
//            var userMock = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();

//            //act
//            var result = _mapperConfig.Map<RegisterUserViewModel>(userMock);

//            //assert
//            result.UserName.Should().Be(userMock.Username);
//        }

//        [Fact]
//        public void TestUpdateUserViewModelMapper()
//        {
//            //arrange
//            var userMock = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();

//            //act
//            var result = _mapperConfig.Map<UpdateUserViewModel>(userMock);

//            //assert
//            result.Name.Should().Be(userMock.Name);
//        }

//        [Fact]
//        public void TestUserViewModelMapper()
//        {
//            //arrange
//            var userMock = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .Create();

//            //act
//            var result = _mapperConfig.Map<UserViewModel>(userMock);

//            //assert
//            result.Username.Should().Be(userMock.Username);
//        }
//    }
//}
