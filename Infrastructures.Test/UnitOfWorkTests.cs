﻿//using Applications;
//using AutoFixture;
//using Domain.Entities;
//using Domain.Test;
//using FluentAssertions;
//using Moq;

//namespace Infrastructures.Test
//{
//    public class UnitOfWorkTests : SetupTest
//    {
//        private readonly IUnitOfWork _unitOfWork;
//        public UnitOfWorkTests()
//        {
//            _unitOfWork = new UnitOfWork(
//                _dbContext,
//                _userRepositoryMock.Object,
//                _noteRepositoryMock.Object,
//                _userTokenRepositoryMock.Object
//                );
//        }

//        [Fact]
//        public async Task TestUnitOfWork()
//        {
//            // arrange
//            var mockData = _fixture.Build<Customer>()
//                                   .Without(x => x.Notes)
//                                   .CreateMany(10)
//                                   .ToList();

//            _userRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(mockData);

//            // act
//            var items = await _unitOfWork.CustomerRepository.GetAllAsync();

//            // assert
//            items.Should().BeEquivalentTo(mockData);
//        }

//    }
//}
