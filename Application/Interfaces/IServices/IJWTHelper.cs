﻿using Domain.Entities;
using Microsoft.Extensions.Configuration;

namespace Application.Interfaces.IServices
{
    public interface IJWTHelper
    {
        string Hash(string password, string salt);
        string Salt();
        bool Verify(string password, string stringVerify);
        string GenerateToken(User user, IConfiguration configuration);
    }
}