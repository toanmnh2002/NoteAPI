﻿using Application.Extensions;
using Application.Models.AccountModels;
using Application.Models.Response;
using Application.Models.UserModels;
using Domain.Entities;

namespace Application.Interfaces.IServices
{
    public interface IUserService
    {
        Task<Response<UserViewModel>> DeleteUser(Guid id);
        Task<Response<Pagination<User>>> FilterAllUser(FilterUserRequest filterUserRequest);
        Task<Response<UserViewModel>> GetUserByIdAsync(Guid id);
        Task<Response<UserViewModel>> GetUserProfile();
        Task<Response<Pagination<UserViewModel>>> GetUsersAsync(int pageIndex = 0, int pageSize = 10);
        Task<Response<string>> LoginAsync(LoginModel model);
        Task<Response<UserViewModel>> RegisterAsync(RegisterModel model);
        //Task SendMailCreateUserAsync(string email, string password);
    }
}