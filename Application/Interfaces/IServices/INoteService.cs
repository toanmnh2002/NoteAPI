﻿using Application.Extensions;
using Application.Models.NoteModels;
using Application.Models.Response;
using Domain.Entities;
using Microsoft.AspNetCore.Http;

namespace Application.Interfaces.IServices
{
    public interface INoteService
    {
        Task<Response<CreateNoteViewModel>> AddNoteAsync(CreateNoteViewModel note);
        Task<Response<NoteViewModel>> DeleteNote(Guid noteId);
        Task<Response<UpdateNoteViewModel>> Update(Guid noteId, UpdateNoteViewModel newNote);
        Task<Response<Pagination<NoteViewModel>>> GetAllAsync(int pageIndex = 0, int pageSize = 10);
        Task<Response<Pagination<Note>>> FilterAllNote(FilterNoteRequest filterNoteRequest);
        Task<Response<Note>> AddImagesToReview(Guid id, List<IFormFile> files);
    }
}
