﻿using Application.Extensions;
using Application.Models.OrderModels;
using Application.Models.Response;

namespace Application.Interfaces.IServices
{
    public interface IOrderService
    {
        Task<Response<OrderModel>> AddOrderAsync(CreateOrderModel model);
        Task<Response<OrderModel>> GetOrderByIdAsync(Guid id);
        Task<Response<Pagination<OrderModel>>> GetOrdersAsync(int pageIndex, int pageSize = 10);
    }
}