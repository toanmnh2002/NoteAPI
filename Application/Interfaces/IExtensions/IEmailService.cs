﻿using Application.Models.EmailModels;

namespace Application.Interfaces.IExtensions
{
    public interface IEmailService
    {
        string GetMailBody(string title = "", string speech = "", string mainContent = "", string alternativeSpeech = "", string alternativeContent = "", string sign = "", string mainContentLink = "");
        Task SendEmail(EmailRequest emailRequest);
        Task SendEmailContent(EmailModel emailModel);
    }
}