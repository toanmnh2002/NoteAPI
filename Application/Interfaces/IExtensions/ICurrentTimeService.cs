﻿namespace Application.Interfaces.IExtensions
{
    public interface ICurrentTimeService
    {
        DateTime GetCurrentTime();
    }
}
