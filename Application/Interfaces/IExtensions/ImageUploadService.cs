﻿using Microsoft.AspNetCore.Http;

namespace Application.Interfaces.IExtensions
{
    public interface ImageUploadService
    {
        Task<List<string>> Upload(ICollection<IFormFile> files);
    }
}
