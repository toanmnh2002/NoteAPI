﻿namespace Application.Interfaces.IExtensions
{
    public interface IClaimService
    {
        Guid? GetCurrentUserId { get; }
        string? GetCurrentUserName { get; }
    }
}
