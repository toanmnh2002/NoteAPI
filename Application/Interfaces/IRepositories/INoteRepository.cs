﻿using Domain.Entities;

namespace Application.Interfaces.IRepositories
{
    public interface INoteRepository : IGenericRepository<Note>
    {
    }
}
