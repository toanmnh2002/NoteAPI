﻿namespace Application.Models.UserModels
{
    public class UpdateUserViewModel
    {
        public string PassWord { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
    }
}
