﻿using Domain.Enum;

namespace Application.Models.UserModels
{
    public class FilterUserRequest
    {
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? Name { get; set; }
        public ICollection<StatusEnum>? Status { get; set; }
        public ICollection<bool>? UserIsDeleted { get; set; }
        public int PageIndex { get; set; } = 0;
        public int PageSize { get; set; } = 10;
    }
}
