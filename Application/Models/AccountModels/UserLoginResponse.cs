﻿namespace Application.Models.AccountModels
{
    public class UserLoginResponse
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }
        public DateTime? ExpireDay { get; set; }
    }
}
