﻿namespace Application.Models.AccountModels
{
    public class RegisterModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
    }
}
