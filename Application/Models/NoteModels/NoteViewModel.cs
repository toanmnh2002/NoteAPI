﻿using Domain.Enum;

namespace Application.Models.NoteModels
{
    public class NoteViewModel
    {
        public Guid NoteId { get; set; }
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public string Content { get; set; }
        public List<string> Image { get; set; }
        public PriorityEnum Priority { get; set; }
        public NoteStatusEnum NoteStatus { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreationDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public string? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
