﻿using Domain.Enum;

namespace Application.Models.NoteModels
{
    public class FilterNoteRequest
    {
        public string? Title { get; set; }
        public string? Descriptions { get; set; }
        public string? Content { get; set; }
        public ICollection<PriorityEnum>? Priority { get; set; }
        public ICollection<NoteStatusEnum>? NoteStatus { get; set; }
        public ICollection<bool>? NoteIsDeleted { get; set; }
        public int PageIndex { get; set; } = 0;
        public int PageSize { get; set; } = 10;
    }
}
