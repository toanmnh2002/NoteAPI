﻿using Domain.Enum;

namespace Application.Models.NoteModels
{
    public class CreateNoteViewModel
    {
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public Guid CustomerId { get; set; }
        public string Content { get; set; }
        public List<string> Image { get; set; }
        public PriorityEnum Priority { get; set; }
    }
}
