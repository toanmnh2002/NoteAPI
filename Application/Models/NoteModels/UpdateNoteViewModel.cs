﻿using Domain.Enum;

namespace Application.Models.NoteModels
{
    public class UpdateNoteViewModel
    {
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public string Content { get; set; }
        public PriorityEnum Priority { get; set; }
    }
}
