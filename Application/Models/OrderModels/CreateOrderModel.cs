﻿namespace Application.Models.OrderModels
{
    public class CreateOrderModel
    {
        public Guid UserId { get; set; }
        public List<CreateOrderDetail>? OrderDetails { get; set; }
    }
    public class CreateOrderDetail
    {
        public Guid NoteId { get; set; }
        public int Quantity { get; set; }
    }

}
