﻿using Domain.Enum;
using System.Text.Json.Serialization;

namespace Application.Models.OrderModels
{
    public class OrderModel
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public OrderStatus OrderStatus { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentMethod { get; set; }
        public decimal TotalPrice { get; set; }
        public Guid UserId { get; set; }
        public ICollection<OrderDetails>? OrderDetails { get; set; }
    }
    public class OrderDetails
    {
        public Guid OrderId { get; set; }
        public Guid NoteId { get; set; }
        public decimal TotalPrice => Price * Quantity;
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
