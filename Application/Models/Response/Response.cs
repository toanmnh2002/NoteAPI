﻿using System.Net;

namespace Application.Models.Response
{
    public class Response<TEntity>
    {
        public HttpStatusCode Code { get; set; }
        public string? Error { get; set; }
        public TEntity? Result { get; set; }
        public Response(TEntity? result)
            => (Code, Result) = (HttpStatusCode.OK, result);

        public Response(string? errors, int statusCode)
            => (Error, Code) = (errors, (HttpStatusCode)statusCode);
    }
}
