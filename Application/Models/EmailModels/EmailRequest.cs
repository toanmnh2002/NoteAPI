﻿using Microsoft.AspNetCore.Http;

namespace Application.Models.EmailModels
{
    public class EmailRequest
    {

        public string Receiver { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public ICollection<IFormFile>? Attachments { get; set; }
    }
}
