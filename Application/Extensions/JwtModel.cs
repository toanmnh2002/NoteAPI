﻿namespace Application.Extensions
{
    public class JwtModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
