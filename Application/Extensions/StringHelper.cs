﻿namespace Application.Extensions
{
    public static class StringHelper
    {
        public static string ToPluralName(this string name)
        {
            var pluralFromName = new string[]
            {
                "o","s","ch","x","sh","z"
            };
            var lastCharInName = name[^2..];
            var addElementPlural = pluralFromName.Any(x => lastCharInName.EndsWith(x));
            return name + addElementPlural;
        }
    }
}
