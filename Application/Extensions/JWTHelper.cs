﻿using Application.Interfaces.IServices;
using Domain.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Application.Extensions
{
    public class JWTHelper : IJWTHelper
    {
        public string Hash(string password, string salt) => BCrypt.Net.BCrypt.HashPassword(password, salt);
        public bool Verify(string password, string stringVerify) => BCrypt.Net.BCrypt.Verify(password, stringVerify);
        public string Salt() => BCrypt.Net.BCrypt.GenerateSalt(12);
        public string GenerateToken(User user, IConfiguration configuration)
        {
            var role = GetRole(user);
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var expiredToken = DateTime.UtcNow.AddMinutes(60);
            var claims = new[]
            {
                new Claim("UserId", user.Id.ToString()),
                new Claim("Email", user.Email),
                new Claim("Name",user.Name),
                new Claim(ClaimTypes.Role, role),
                new Claim(JwtRegisteredClaimNames.Exp,expiredToken.ToString("yyyy/MM/dd hh:mm:ss"),ClaimValueTypes.String),
            };
            var token = new JwtSecurityToken(
                    claims: claims,
                    expires: expiredToken,
                    signingCredentials: credentials
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        private string GetRole(User user)
        {
            var a = user.Role;
            var role = (int)a switch
            {
                0 => "Admin",
                1 => "Customer",
                _ => "Unknown"
            };
            return role;
        }
    }
}
