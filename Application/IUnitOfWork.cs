﻿using Application.Interfaces.IRepositories;

namespace Application
{
    public interface IUnitOfWork
    {
        public INoteRepository NoteRepository { get; }
        public IUserRepository UserRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public Task<int> SaveChangesAsync();
    }
}
