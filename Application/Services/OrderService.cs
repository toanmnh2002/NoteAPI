﻿using Application.Extensions;
using Application.Interfaces.IExtensions;
using Application.Interfaces.IServices;
using Application.Models.OrderModels;
using Application.Models.Response;
using AutoMapper;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IClaimService _claimService;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper, IClaimService claimService)
        => (_unitOfWork, _mapper, _claimService) = (unitOfWork, mapper, claimService);

        public async Task<Response<Pagination<OrderModel>>> GetOrdersAsync(int pageIndex, int pageSize = 10)
        {
            var data = await _unitOfWork.OrderRepository.GetAllAsync(pageIndex, pageSize, x => x.Include(x => x.OrderDetails!));

            if (data is null || data.Items.Count is 0 || !data.Items.Any())
                return new Response<Pagination<OrderModel>>("List is empty!", 404);

            var result = _mapper.Map<Pagination<OrderModel>>(data);

            return new Response<Pagination<OrderModel>>(result);
        }

        public async Task<Response<OrderModel>> GetOrderByIdAsync(Guid id)
        {
            var data = await _unitOfWork.OrderRepository.GetByIdAsync(id);

            if (data is null)
                return new Response<OrderModel>("Not found order!", 404);

            var result = _mapper.Map<OrderModel>(data);
            return new Response<OrderModel>(result);
        }

        public async Task<Response<OrderModel>> AddOrderAsync(CreateOrderModel model)
        {
            var currentUser = _claimService.GetCurrentUserId;
            if (currentUser is null || currentUser == Guid.Empty)
                return new Response<OrderModel>("Not login yet!", 404);

            if (model.OrderDetails?.Count is 0 || model.OrderDetails is null || !model.OrderDetails.Any())
            {
                return new Response<OrderModel>("Order details is empty!", 404);
            }

            var order = _mapper.Map<Order>(model);
            order.UserId = currentUser.Value;
            order.PaymentMethod = "momo";
            order.PaymentStatus = "pending";
            decimal price = 0;
            var orderDetails = _mapper.Map<List<OrderDetail>>(model.OrderDetails);
            var priceTasks = model.OrderDetails.Select(async x =>
            {
                return await CalculatePrice((List<OrderDetail>)order.OrderDetails!);
            });

            var prices = await Task.WhenAll(priceTasks);
            price = prices.Sum();

            order.TotalPrice = price;
            _mapper.Map(model.OrderDetails, order.OrderDetails);

            await _unitOfWork.OrderRepository.AddEntityAsync(order);

            var isSuccess = await _unitOfWork.SaveChangesAsync();

            if (isSuccess > 0)
            {
                var result = _mapper.Map<OrderModel>(order);
                var results = _mapper.Map<List<OrderDetail>>(result.OrderDetails);
                await DecreaseQuantity(results);
                return new Response<OrderModel>(result);
            }

            return new Response<OrderModel>("Failed to add order!", 500);
        }

        private async Task<decimal> CalculatePrice(List<OrderDetail> orderDetails)
        {
            decimal totalPrice = 0;
            foreach (var obj in orderDetails)
            {
                var note = await _unitOfWork.NoteRepository.GetEntityByCondition(x => x.Id == obj.NoteId);

                if (note is null)
                {
                    throw new Exception("Note is not found!");
                }

                var itemPrice = note.Price * obj.Quantity;
                totalPrice += itemPrice;
            }
            return totalPrice;
        }

        private async Task<int> DecreaseQuantity(List<OrderDetail> orderDetails)
        {
            int quantity = 0;
            foreach (var x in orderDetails)
            {
                var note = await _unitOfWork.NoteRepository.GetByIdAsync(x.NoteId);

                if (note is null)
                {
                    throw new Exception("Note is not found!");
                }

                note.Quantity -= x.Quantity;
                quantity += x.Quantity;
            }
            return quantity;
        }
    }
}
