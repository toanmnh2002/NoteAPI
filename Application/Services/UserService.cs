﻿using Application.Extensions;
using Application.Interfaces.IExtensions;
using Application.Interfaces.IServices;
using Application.Models.AccountModels;
using Application.Models.Response;
using Application.Models.UserModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enum;
using Microsoft.Extensions.Configuration;
using System.Linq.Expressions;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly IJWTHelper _jwtService;
        private readonly IClaimService _claimService;

        public UserService(IUnitOfWork unitOfWork, IConfiguration configuration, IMapper mapper, IJWTHelper jWTHelper, IClaimService claimService)
     => (_unitOfWork, _configuration, _mapper, _jwtService, _claimService) = (unitOfWork, configuration, mapper, jWTHelper, claimService);

        public async Task<Response<Pagination<User>>> FilterAllUser(FilterUserRequest filterUserRequest)
        {
            Expression<Func<User, bool>> predicate = x => true;
            if (!string.IsNullOrEmpty(filterUserRequest.Name))
            {
                Expression<Func<User, bool>> subExpression = x => x.Name
                                                                        .ToLower()
                                                                        .Contains(
                                                                            filterUserRequest.Name.ToLower()
                                                                            );
                predicate = ExpressionHelper<User>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (!string.IsNullOrEmpty(filterUserRequest.Email))
            {
                Expression<Func<User, bool>> subExpression = x => x.Email
                                                                        .ToLower()
                                                                        .Contains(
                                                                            filterUserRequest.Email.ToLower()
                                                                            );
                predicate = ExpressionHelper<User>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (filterUserRequest.Status is not null && filterUserRequest.Status.Any())
            {
                foreach (var item in filterUserRequest.Status)
                {
                    Expression<Func<User, bool>> subExpression = user => user.Status == item;
                    predicate = ExpressionHelper<User>.ExpressionCombineAndAlso(predicate, subExpression);
                }
            }
            if (filterUserRequest.UserIsDeleted is not null && filterUserRequest.UserIsDeleted.Any())
            {
                foreach (var item in filterUserRequest.UserIsDeleted)
                {
                    Expression<Func<User, bool>> subExpression = user => user.IsDeleted == item;
                    predicate = ExpressionHelper<User>.ExpressionCombineAndAlso(predicate, subExpression);
                }
            }
            var result = await _unitOfWork.UserRepository.
                Filter(predicate,
                pageIndex: filterUserRequest.PageIndex,
                pageSize: filterUserRequest.PageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);

            return new Response<Pagination<User>>(result);
        }

        public async Task<Response<UserViewModel>> GetUserByIdAsync(Guid id)
        {
            var data = await _unitOfWork.UserRepository.GetByIdAsync(id);

            if (data is null)
                return new Response<UserViewModel>("Not found user!", 404);

            var result = _mapper.Map<UserViewModel>(data);
            return new Response<UserViewModel>(result);
        }

        public async Task<Response<UserViewModel>> GetUserProfile()
        {
            var currentUserId = _claimService.GetCurrentUserId;
            if (currentUserId is null || currentUserId == Guid.Empty)
                return new Response<UserViewModel>("Not login yet!", 404);

            var data = await _unitOfWork.UserRepository.GetByIdAsync(currentUserId.Value);

            if (data is null)
                return new Response<UserViewModel>("Not found user!", 404);

            var result = _mapper.Map<UserViewModel>(data);
            return new Response<UserViewModel>(result);
        }


        public async Task<Response<Pagination<UserViewModel>>> GetUsersAsync(int pageIndex = 1, int pageSize = 10)
        {
            var data = await _unitOfWork.UserRepository.GetAllAsync(pageIndex, pageSize);

            if (data is null || data.Items.Count is 0)
                return new Response<Pagination<UserViewModel>>("List is empty!", 404);

            var result = _mapper.Map<Pagination<UserViewModel>>(data);

            return new Response<Pagination<UserViewModel>>(result);
        }

        public async Task<Response<string>> LoginAsync(LoginModel model)
        {
            var userLogin = await _unitOfWork.UserRepository.GetEntityByCondition(x => x.Email == model.Email);

            if (userLogin is null || !_jwtService.Verify(model.Password, userLogin.Password))
            {
                return new Response<string>("Email or password is incorrect!", 404);
            }

            var result = _jwtService.GenerateToken(userLogin, _configuration);
            return new Response<string>(result);
        }

        public async Task<Response<UserViewModel>> RegisterAsync(RegisterModel model)
        {
            var user = _mapper.Map<User>(model);
            var salt = _jwtService.Salt();
            user.Password = _jwtService.Hash(user.Password, salt);

            await _unitOfWork.UserRepository.AddEntityAsync(user);

            var isSuccess = await _unitOfWork.SaveChangesAsync();

            return isSuccess > 0
                ? new Response<UserViewModel>(_mapper.Map<UserViewModel>(user))
                : new Response<UserViewModel>("Register user fail!", 400);
        }

        public async Task<Response<UserViewModel>> DeleteUser(Guid id)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(id);

            if (user is null)
                return new Response<UserViewModel>("Not found user!", 404);

            _unitOfWork.UserRepository.SoftRemove(user);

            var isSuccess = await _unitOfWork.SaveChangesAsync();

            return isSuccess > 0
                ? new Response<UserViewModel>(_mapper.Map<UserViewModel>(user))
                : new Response<UserViewModel>("Delete user fail!", 400);
        }
        //public async Task SendMailCreateUserAsync(string email, string password)
        //{
        //    var to = new List<string> { email };
        //    var title = "Welcome to TOAN NGUYEN API";
        //    var speech = "Greetings,\nWelcome to my project and thank you for caring my project. This is your login password:";
        //    var mainContent = password;
        //    var sign = "Nguyen Manh Toan";
        //    var body = _emailService.GetMailBody(title, speech, mainContent, sign: sign);
        //    var subject = "Welcome to TOAN NGUYEN API";
        //    var mailData = new EmailModel(to, subject, body);
        //    await _emailService.SendEmailContent(mailData);
        //}
    }
}
