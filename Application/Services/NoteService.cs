﻿using Application.Extensions;
using Application.Interfaces.IExtensions;
using Application.Interfaces.IServices;
using Application.Models.NoteModels;
using Application.Models.Response;
using AutoMapper;
using Domain.Entities;
using Domain.Enum;
using Microsoft.AspNetCore.Http;
using System.Linq.Expressions;

namespace Application.Services
{
    public class NoteService : INoteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IClaimService _claimService;
        private readonly ImageUploadService _image;

        public NoteService(IUnitOfWork unitOfWork, IMapper mapper, IClaimService claimService, ImageUploadService image)
            => (_unitOfWork, _mapper, _claimService, _image) = (unitOfWork, mapper, claimService, image);

        public async Task<Response<CreateNoteViewModel>> AddNoteAsync(CreateNoteViewModel model)
        {
            var currentUser = _claimService.GetCurrentUserId;

            if (currentUser == Guid.Empty || !currentUser.HasValue) return new Response<CreateNoteViewModel>("List is empty!", 404);

            var note = _mapper.Map<Note>(model);

            note.NoteStatus = NoteStatusEnum.Doing;
            note.Priority = PriorityEnum.Unknow;

            note.CreationDate = DateTime.UtcNow;
            note.CreatedBy = _claimService.GetCurrentUserName;

            await _unitOfWork.NoteRepository.AddEntityAsync(note);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;

            return isSuccess ? new Response<CreateNoteViewModel>(_mapper.Map<CreateNoteViewModel>(note)) : new Response<CreateNoteViewModel>("Create Note Fail!", 400);
        }

        public async Task<Response<NoteViewModel>> DeleteNote(Guid noteId)
        {
            var note = await _unitOfWork.NoteRepository.GetEntityByCondition(x => x.Id == noteId);

            if (note is null) return new Response<NoteViewModel>("Invalid NoteId!", 404);

            note.DeletionDate = DateTime.UtcNow;
            note.DeletedBy = _claimService.GetCurrentUserName;

            _unitOfWork.NoteRepository.Remove(note);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;

            var result = _mapper.Map<NoteViewModel>(note);
            return isSuccess ? new Response<NoteViewModel>(result) : new Response<NoteViewModel>("Delete Note Fail!", 400);
        }

        public async Task<Response<UpdateNoteViewModel>> Update(Guid noteId, UpdateNoteViewModel newNote)
        {
            var note = await _unitOfWork.NoteRepository.GetEntityByCondition(x => x.Id == noteId);

            if (note is null) return new Response<UpdateNoteViewModel>("Note is not found!", 404);

            _mapper.Map(newNote, note);

            note.ModificationDate = DateTime.UtcNow;
            note.ModificatedBy = _claimService.GetCurrentUserName;
            _unitOfWork.NoteRepository.Update(note);

            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;

            var result = _mapper.Map<UpdateNoteViewModel>(note);
            return isSuccess ? new Response<UpdateNoteViewModel>(result) : new Response<UpdateNoteViewModel>("Update Note Fail!", 400);
        }

        public async Task<Response<Pagination<NoteViewModel>>> GetAllAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listNote = await _unitOfWork.NoteRepository
                .Filter(
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);

            var result = _mapper.Map<Pagination<NoteViewModel>>(listNote);

            return listNote is not null ? new Response<Pagination<NoteViewModel>>(result) : new Response<Pagination<NoteViewModel>>("No note is found!", 404); ;
        }

        public async Task<Response<Note>> AddImagesToReview(Guid id, List<IFormFile> files)
        {
            var note = await _unitOfWork.NoteRepository.GetByIdAsync(id);

            if (note is null) return new Response<Note>("Not found", 404);

            List<string> a = await _image.Upload(files);
            note.Image.AddRange(a);
            _unitOfWork.NoteRepository.Update(note);
            var isSuccess = await _unitOfWork.SaveChangesAsync() > 0;
            return isSuccess ? new Response<Note>(note) : new Response<Note>("Update Note Fail!", 400);
        }

        public async Task<Response<Pagination<Note>>> FilterAllNote(FilterNoteRequest filterNoteRequest)
        {
            Expression<Func<Note, bool>> predicate = x => true;
            if (!string.IsNullOrEmpty(filterNoteRequest.Title))
            {
                Expression<Func<Note, bool>> subExpression = x => x.Title
                                                                        .ToLower()
                                                                        .Contains(
                                                                            filterNoteRequest.Title.ToLower()
                                                                            );
                predicate = ExpressionHelper<Note>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (!string.IsNullOrEmpty(filterNoteRequest.Descriptions))
            {
                Expression<Func<Note, bool>> subExpression = x => x.Descriptions
                                                                        .ToLower()
                                                                        .Contains(
                                                                            filterNoteRequest.Descriptions.ToLower()
                                                                            );
                predicate = ExpressionHelper<Note>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (!string.IsNullOrEmpty(filterNoteRequest.Content))
            {
                Expression<Func<Note, bool>> subExpression = x => filterNoteRequest.Content
                                                                        .ToLower()
                                                                        .Contains(
                                                                            filterNoteRequest.Content.ToLower()
                                                                            );
                predicate = ExpressionHelper<Note>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (filterNoteRequest.NoteStatus != null && filterNoteRequest.NoteStatus.Any())
            {
                foreach (var item in filterNoteRequest.NoteStatus)
                {
                    Expression<Func<Note, bool>> subExpression = note => note.NoteStatus == item;
                    predicate = ExpressionHelper<Note>.ExpressionCombineAndAlso(predicate, subExpression);
                }
            }
            if (filterNoteRequest.Priority != null && filterNoteRequest.Priority.Any())
            {
                foreach (var item in filterNoteRequest.Priority)
                {
                    Expression<Func<Note, bool>> subExpression = note => note.Priority == item;
                    predicate = ExpressionHelper<Note>.ExpressionCombineAndAlso(predicate, subExpression);
                }
            }
            if (filterNoteRequest.NoteIsDeleted != null && filterNoteRequest.NoteIsDeleted.Any())
            {
                foreach (var item in filterNoteRequest.NoteIsDeleted)
                {
                    Expression<Func<Note, bool>> subExpression = note => note.IsDeleted == item;
                    predicate = ExpressionHelper<Note>.ExpressionCombineAndAlso(predicate, subExpression);
                }
            }
            var result = await _unitOfWork.NoteRepository.
                Filter(predicate,
                pageIndex: filterNoteRequest.PageIndex,
                pageSize: filterNoteRequest.PageSize,
                sortColumn: x => x.CreationDate,
                sortDirection: SortDirection.Descending);

            if (result.Items.Count == 0) return new Response<Pagination<Note>>("No note is found!", 404);

            return new Response<Pagination<Note>>(result);
        }
        public async Task<Response<Pagination<NoteViewModel>>> FilterNoteAsync(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = new Pagination<NoteViewModel>();
            var data = new Pagination<Note>();

            var priorityEnum = Enum.GetValues(typeof(PriorityEnum))
                       .Cast<PriorityEnum>()
                       .Select(x => x.ToString())
                       .ToList();

            var noteStatus = Enum.GetValues(typeof(NoteStatusEnum))
                       .Cast<NoteStatusEnum>()
                       .Select(x => x.ToString())
                       .ToList();

            if (!string.IsNullOrWhiteSpace(query))
            {
                data = await _unitOfWork.NoteRepository
                    .Filter(
                    x => x.Title.Contains(query)
                    || x.Descriptions.Contains(query)
                    || x.Content.Contains(query)
                    || (priorityEnum.Contains(query) && x.Priority == (PriorityEnum)Enum.Parse(typeof(PriorityEnum), query))
                    || (noteStatus.Contains(query) && x.NoteStatus == (NoteStatusEnum)Enum.Parse(typeof(NoteStatusEnum), query))
                    ,
                        null, pageIndex, pageSize);
                result = _mapper.Map<Pagination<NoteViewModel>>(data);
            }
            else
            {
                data = await _unitOfWork.NoteRepository
                    .Filter(null, null,
                        pageIndex,
                        pageSize,
                        x => x.Id,
                        SortDirection.Descending);
                result = _mapper.Map<Pagination<NoteViewModel>>(data);
            }

            return new Response<Pagination<NoteViewModel>>(result);
        }

    }
}


