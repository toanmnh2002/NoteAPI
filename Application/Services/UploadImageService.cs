﻿using Application.Interfaces.IExtensions;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;

namespace Application.Services
{
    public class UploadImageService : ImageUploadService
    {
        const string Cloud = "di7yhx8nt";
        const string ApiKey = "419863164343379";
        const string ApiSecret = "unDWyqHiMg_tsCHnrxOX5MMGdl8";

        private readonly Cloudinary _cloudinary;

        public UploadImageService()
        {
            var account = new Account(Cloud, ApiKey, ApiSecret);
            _cloudinary = new Cloudinary(account);
            _cloudinary.Api.Secure = true;
        }

        public async Task<List<string>> Upload(ICollection<IFormFile> files)
        {
            var imageUrls = new List<string>();

            foreach (var file in files)
            {
                using var memoryStream = new MemoryStream();
                await file.CopyToAsync(memoryStream);
                memoryStream.Position = 0;

                var uploadparams = new ImageUploadParams
                {
                    File = new FileDescription(file.FileName, memoryStream),
                };

                var result = _cloudinary.Upload(uploadparams);

                if (result.Error != null)
                {
                    throw new Exception($"Cloudinary error occured: {result.Error.Message}");
                }

                imageUrls.Add(result.SecureUrl.ToString());
            }

            return imageUrls;
        }
    }
}
