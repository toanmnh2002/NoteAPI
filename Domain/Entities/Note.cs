﻿using Domain.Enum;

namespace Domain.Entities
{
    public class Note : BaseEntity
    {
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public string Content { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public List<string>? Image { get; set; }
        public PriorityEnum Priority { get; set; }
        public NoteStatusEnum NoteStatus { get; set; }
        public ICollection<OrderDetail>? OrderDetails { get; set; }
    }
}
