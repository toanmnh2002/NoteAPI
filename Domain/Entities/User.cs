﻿using Domain.Enum;

namespace Domain.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string? ImageURL { get; set; }
        public StatusEnum Status { get; set; }
        public RoleName Role { get; set; }
        public ICollection<Note>? Notes { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
