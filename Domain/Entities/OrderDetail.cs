﻿namespace Domain.Entities
{
    public class OrderDetail : BaseEntity
    {
        public Guid OrderId { get; set; }
        public Guid NoteId { get; set; }
        public int Quantity { get; set; }
        public Order Order { get; set; }
        public Note Note { get; set; }
    }
}
