namespace Domain.Enum
{
    public enum SortDirection
    {
        Ascending,
        Descending
    }

}
