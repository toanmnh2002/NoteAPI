﻿using Application;
using Application.Interfaces.IExtensions;
using Application.Interfaces.IRepositories;
using Application.Interfaces.IServices;
using AutoFixture;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using Persistence;
using System.Reflection;

namespace Domain.Test
{
    public class SetupTest : IDisposable
    {
        protected readonly IMapper _mapperConfig;
        protected readonly Fixture _fixture;
        protected readonly Mock<IUnitOfWork> _unitOfWorkMock;
        protected readonly Mock<ICurrentTimeService> _currentTimeMock;
        protected readonly Mock<IClaimService> _claimServiceMock;
        protected readonly Mock<INoteService> _noteServiceMock;
        protected readonly Mock<INoteRepository> _noteRepositoryMock;
        protected readonly AppDbContext _dbContext;

        public SetupTest()
        {
            var config = TypeAdapterConfig.GlobalSettings;
            config.Scan(Assembly.GetExecutingAssembly());

            _mapperConfig = new Mapper(config);
            _fixture = new Fixture();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _currentTimeMock = new Mock<ICurrentTimeService>();
            _claimServiceMock = new Mock<IClaimService>();
            _noteServiceMock = new Mock<INoteService>();
            _noteRepositoryMock = new Mock<INoteRepository>();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            _dbContext = new AppDbContext(options);

            _currentTimeMock.Setup(x => x.GetCurrentTime()).Returns(DateTime.UtcNow);
            _claimServiceMock.Setup(x => x.GetCurrentUserId).Returns(Guid.Empty);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
