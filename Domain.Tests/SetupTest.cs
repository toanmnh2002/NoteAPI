﻿using Application;
using Application.Interfaces.IExtensions;
using Application.Interfaces.IRepositories;
using Application.Interfaces.IServices;
using AutoFixture;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using Persistence;

namespace Domain.Tests
{
    public class SetupTest : IDisposable
    {
        protected readonly Fixture _fixture;

        protected readonly Mock<IMapper> _mapperMock;
        protected readonly Mock<IUnitOfWork> _unitOfWorkMock;
        protected readonly Mock<IConfiguration> _configurationMock;


        protected readonly AppDbContext _context;
        protected readonly Mock<AppDbContext> _mockContext;
        protected readonly Mock<IUserRepository> _userRepositoryMock;
        protected readonly Mock<INoteRepository> _noteRepositoryMock;

        protected readonly Mock<IUserService> _userServiceMock;
        protected readonly Mock<INoteService> _noteServiceMock;
        protected readonly Mock<IEmailService> _emailServiceMock;
        protected readonly Mock<IClaimService> _claimServiceMock;
        protected readonly Mock<IJWTHelper> _jwtServiceMock;
        protected readonly Mock<ImageUploadService> _uploadServiceMock;

        public SetupTest()
        {
            _fixture = new Fixture();

            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _mapperMock = new Mock<IMapper>();
            _configurationMock = new Mock<IConfiguration>();

            _userRepositoryMock = new Mock<IUserRepository>();
            _noteRepositoryMock = new Mock<INoteRepository>();

            _userServiceMock = new Mock<IUserService>();
            _noteServiceMock = new Mock<INoteService>();

            _claimServiceMock = new Mock<IClaimService>();
            _jwtServiceMock = new Mock<IJWTHelper>();
            _uploadServiceMock = new Mock<ImageUploadService>();
            _emailServiceMock = new Mock<IEmailService>();

            var options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "ToanNguyenManh".ToString())
            .Options;
            _context = new AppDbContext(options);
            _mockContext = new Mock<AppDbContext>();
        }

        public void Dispose() => _context.Dispose();

    }

}
