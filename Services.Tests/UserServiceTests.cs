using Application;
using Application.Interfaces.IExtensions;
using Application.Interfaces.IServices;
using Application.Services;
using AutoMapper;
using Domain.Entities;
using Domain.Tests;
using Moq;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace Services.Tests
{
    public class UserServiceTests : SetupTest
    {
        private readonly UserService _userService;

        public UserServiceTests()
        {
            _userService = new UserService(_unitOfWorkMock.Object, _configurationMock.Object, _mapperMock.Object, _jwtServiceMock.Object, _claimServiceMock.Object);
        }

        //[Fact]
        //public async Task GetUserProfile_WithValidUserId_ReturnsUserViewModel()
        //{
        //    // Arrange
        //    var claimService = new Mock<IClaimService>();
        //    claimService.Setup(c => c.GetCurrentUserId()).Returns(Guid.NewGuid());

        //    var unitOfWork = new Mock<IUnitOfWork>();
        //    unitOfWork.Setup(u => u.UserRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(new User());

        //    var mapper = new Mock<IMapper>();

        //    var userProfileService = new UserProfileService(claimService.Object, unitOfWork.Object, mapper.Object);

        //    // Act
        //    var result = await userProfileService.GetUserProfile();

        //    // Assert
        //    Assert.NotNull(result.Data);
        //    Assert.IsType<UserViewModel>(result.Data);
        //}

        //[Fact]
        //public async Task GetUserProfile_WithInvalidUserId_ReturnsErrorResponse()
        //{
        //    // Arrange
        //    var claimService = new Mock<IClaimService>();
        //    claimService.Setup(c => c.GetCurrentUserId).Returns(Guid.Empty);

        //    var unitOfWork = new Mock<IUnitOfWork>();
        //    var mapper = new Mock<IMapper>();

        //    var userProfileService = new UserProfileService(claimService.Object, unitOfWork.Object, mapper.Object);

        //    // Act
        //    var result = await userProfileService.GetUserProfile();

        //    // Assert
        //    Assert.Equal("Not login yet!", result.Message);
        //    Assert.Equal(404, result.StatusCode);
        //    Assert.Null(result.Data);
        //}

        [Fact]
        public async Task GetUserProfile_WithNonExistingUser_ReturnsErrorResponse()
        {
            // Arrange
            var claimService = new Mock<IClaimService>();
            claimService.Setup(c => c.GetCurrentUserId).Returns(Guid.NewGuid());

            var unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(u => u.UserRepository.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((User)null);

            var jwtService = new Mock<IJWTHelper>();

            var mapper = new Mock<IMapper>();
            var configuration = new Mock<IConfiguration>();

            var userProfileService = new UserService(unitOfWork.Object, configuration.Object, mapper.Object, jwtService.Object, claimService.Object);

            // Act
            var result = await userProfileService.GetUserProfile();

            // Assert
            Assert.Equal("Not found user!", result.Error);
            //Assert.Equal(404.ToString(), result.Code.ToString());
            Assert.Null(result.Result);
        }


    }
}
