﻿using APIs.Services;
using Application.Extensions;
using Application.Interfaces.IExtensions;
using Application.Interfaces.IServices;
using Application.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text;

namespace APIs
{
    public static class DependencyInjection
    {
        public static IServiceCollection APIsServices(this IServiceCollection services, IConfiguration configuration)
        {
            #region ServicesConfig
            services.AddHttpContextAccessor();
            services.AddScoped<IClaimService, ClaimService>();
            services.AddSingleton<ICurrentTimeService, CurrentTimeService>();
            //services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<ImageUploadService, UploadImageService>();
            services.AddScoped<IJWTHelper, JWTHelper>();

            //services.Configure<MailSection>(configuration.GetSection("MailSettings"));
            #endregion


            services.AddHttpClient("EmailService", client =>
            {
                client.BaseAddress = new Uri("http://localhost:5005/");
                // Add any other custom configuration options for the HttpClient
            });

            #region SwaggerConfig
            services.AddSwaggerGen(
               c =>
               {
                   c.SwaggerDoc("v1", new OpenApiInfo
                   {
                       Title = "Note App",
                       Version = "v1",
                       Description = "This is the API of Toan Nguyen",
                       Contact = new OpenApiContact
                       {
                           Url = new Uri("https://google.com")
                       }
                   });
                   var xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                   c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName));
                   c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                   {
                       Type = SecuritySchemeType.Http,
                       In = ParameterLocation.Header,
                       BearerFormat = "JWT",
                       Scheme = "Bearer",
                       Description = "Please input your token"
                   });
                   c.AddSecurityRequirement(new OpenApiSecurityRequirement
                   {
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference=new OpenApiReference
                                    {
                                        Type=ReferenceType.SecurityScheme,
                                        Id="Bearer"
                                    }
                                },
                                new string[]{}
                            }
                   });

               });
            #endregion

            #region AuthenConfig
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = new SymmetricSecurityKey
                    (Encoding.UTF8.GetBytes(configuration["Jwt:Key"]!)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
            }
                );
            #endregion

            #region Others
            services.AddAuthorization();
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            #endregion

            return services;
        }
    }
}
