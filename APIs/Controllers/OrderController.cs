﻿using Application.Interfaces.IServices;
using Application.Models.OrderModels;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService) => _orderService = orderService;

        [HttpPost]
        public async Task<ActionResult> CreateOrder([FromBody] CreateOrderModel model)
        {
            var order = await _orderService.AddOrderAsync(model);
            return Ok(order);
        }

        [HttpGet]
        public async Task<ActionResult> GetOrders(int pageIndex = 1, int pageSize = 10)
        {
            var orders = await _orderService.GetOrdersAsync(pageIndex, pageSize);
            return Ok(orders);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetOrderById([FromRoute] Guid id)
        {
            var order = await _orderService.GetOrderByIdAsync(id);
            return Ok(order);
        }

        //[HttpDelete("{id}")]
        //public async Task<ActionResult> DeleteOrder([FromRoute] Guid id)
        //{
        //    var order = await _orderService.DeleteOrder(id);
        //    return Ok(order);
        //}
    }
}
