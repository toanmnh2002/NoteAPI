﻿using Application.Interfaces.IServices;
using Application.Models.AccountModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService) => _userService = userService;

        [HttpPost]
        public async Task<ActionResult> Register([FromBody] RegisterModel model)
        {
            var user = await _userService.RegisterAsync(model);
            return Ok(user);
        }

        [HttpPost]
        public async Task<ActionResult> Login([FromBody] LoginModel model)
        {
            var user = await _userService.LoginAsync(model);
            return Ok(user);
        }

        [HttpGet]
        public async Task<ActionResult> GetUsers(int pageIndex = 1, int pageSize = 10)
        {
            var users = await _userService.GetUsersAsync(pageIndex, pageSize);
            return Ok(users);
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "Admin,Customer")]
        public async Task<ActionResult> GetUserById([FromRoute] Guid id)
        {
            var user = await _userService.GetUserByIdAsync(id);
            return Ok(user);
        }


        [HttpGet]
        public async Task<ActionResult> GetUserProfile()
        {
            var user = await _userService.GetUserProfile();
            return Ok(user);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteUser([FromRoute] Guid id)
        {
            var user = await _userService.DeleteUser(id);
            return Ok(user);
        }

    }
}

