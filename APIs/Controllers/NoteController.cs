﻿using Application.Extensions;
using Application.Interfaces.IServices;
using Application.Models.NoteModels;
using Application.Models.Response;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace APIs.Controllers
{
    public class NoteController : BaseController
    {
        private readonly INoteService _noteService;
        public NoteController(INoteService noteService) => _noteService = noteService;

        [HttpGet]
        public async Task<ActionResult<Response<Pagination<NoteViewModel>>>> GetUserNotes(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _noteService.GetAllAsync(pageIndex, pageSize);
            if (response.Code is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult<Response<CreateNoteViewModel>>> AddNewNoteAsync(CreateNoteViewModel note)
        {
            var response = await _noteService.AddNoteAsync(note);
            if (response.Code is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPut]
        public async Task<ActionResult<Response<UpdateNoteViewModel>>> UpdateNoteAsync(Guid noteId, UpdateNoteViewModel note)
        {
            var response = await _noteService.Update(noteId, note);
            if (response.Code is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpDelete]
        public async Task<ActionResult<Response<NoteViewModel>>> DeleteNoteAsync(Guid noteId)
        {
            var response = await _noteService.DeleteNote(noteId);
            if (response.Code is not HttpStatusCode.OK && response.Result is null)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpGet("Filteration/Note")]
        [AllowAnonymous]
        public async Task<ActionResult<Response<Pagination<Note>>>> FilterAllNote([FromQuery] FilterNoteRequest filterNoteRequest)
        {
            var response = await _noteService.FilterAllNote(filterNoteRequest);
            if (response.Code is not HttpStatusCode.OK)
                return BadRequest(response);
            return Ok(response);
        }

        [HttpPost("{id}/images")]
        [AllowAnonymous]
        public async Task<ActionResult> AddImagesToReview(Guid id, [FromForm] List<IFormFile> files)
        {
            var review = await _noteService.AddImagesToReview(id, files);
            return Ok(review);
        }
    }
}
