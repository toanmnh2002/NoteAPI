﻿using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
    }
}
