﻿using Application.Interfaces.IExtensions;
using System.Security.Claims;

namespace APIs.Services
{
    public class ClaimService : IClaimService
    {
        public ClaimService(IHttpContextAccessor httpContextAccessor)
        {
            //var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("userID");
            var Id = httpContextAccessor.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "UserId")?.Value;
            var name = httpContextAccessor.HttpContext?.User?.FindFirstValue("Name");
            GetCurrentUserId = string.IsNullOrWhiteSpace(Id) ? Guid.Empty : Guid.Parse(Id);
            GetCurrentUserName = string.IsNullOrWhiteSpace(name) ? string.Empty : name;
        }
        public Guid? GetCurrentUserId { get; }
        public string? GetCurrentUserName { get; }
    }
}
